#!/usr/bin/expect

set DIR [lindex $argv 0]
set USER [lindex $argv 1]
set PASS [lindex $argv 2]
set NUM [lindex $argv 3]

# timeout 10 hours
set timeout 36000

spawn bash -c "ssh $USER@127.0.0.1"
expect "password: "  { send "$PASS\r" }
expect "$ "  { send "export APPLICATION_VAR=VAR1\r" }
expect "$ "  { send "cd $DIR\r" }
expect "$ "  { send "./application -a\r" }
# Test application menu
expect "$ "  { send "./application --menu\r" }
expect "#? "  { send "1\r" }
expect "#? "  { send "2\r" }
expect "#? "  { send "3\r" }
expect "#? "  { send "4\r" }


for {set idx 0} {$idx < $NUM} {incr idx} {
    puts "\rTEST: Loop $idx\r"
    sleep 1
    expect "$ "  { send "./application --menu\r" }
    expect "#? "  { send "1\r" }
    expect "#? "  { send "2\r" }
    expect "#? "  { send "3\r" }
    expect "#? "  { send "4\r" }
}

expect "$ "  { send "exit\r" }

interact


