# Expect

## Description

Expect is a tool for automating interactive applications such as ssh, minicom, custom menus, etc.

## Example

### Install expect

```bash
sudo apt-get install expect
```

### Download

```bash
git clone git@bitbucket.org:manuelleiva/examples.git
```

### Run example

```bash
./expect_script.sh $(pwd) $USER pass 2
```
